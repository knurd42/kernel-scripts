# -*- coding: utf-8 -*-
# SPDX-License-Identifier: MIT
# Copyright (C) 2023 by Thorsten Leemhuis
__author__ = 'Thorsten Leemhuis <linux@leemhuis.info>'

import datetime
import git
import logging
import pathlib
import re
import sys

### config section: START
git_remotenames = {
    'mainline': 'mainline',
    'next': 'next',
}
nexttree_age = {
    'min': 5,
    'max': 8,
}
#logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.DEBUG)
### config section: END

COMMITS_OF_INTEREST = []
DATE_NOW = datetime.datetime.now()
GITPYREPO = None

# check if a commit is in a branch by comparing the git summary and author; used to detect commits
# that were rebased or to be found in both mainline and next:
def is_duplicate_in_other_branch(provided_commit, branchname):
    # avoid characters can cause trouble for git:
    summary = provided_commit.summary.replace('[', '.').replace(']', '.')
    for result in GITPYREPO.git.log("--pretty=%h", '--since=20 weeks ago', "--grep=%s" % summary, branchname).splitlines():
        found_commit = GITPYREPO.commit(result)
        if found_commit.summary == provided_commit.summary and found_commit.author == provided_commit.author:
            return True
    return False

def is_part_of_branch(commit, branchname):
    try:
        GITPYREPO.git.merge_base('--is-ancestor', commit.hexsha, branchname)
        return True
    except git.exc.GitCommandError as err:
        return False


# check if commits have been in next for the minimum time.
def is_too_young(commit, is_at_least_in_next):
    try:
        GITPYREPO.git.merge_base('--is-ancestor', commit.hexsha, is_at_least_in_next)
        return False
    except git.exc.GitCommandError:
        # special case, mainly for mm due to frequent rebasing and use of quilt
        if is_duplicate_in_other_branch(commit, is_at_least_in_next):
            # check if the author date is old enough, as it might be a newer patch version
            date_distance = DATE_NOW - datetime.datetime.utcfromtimestamp(commit.authored_date)
            if date_distance.days < nexttree_age['max']:
                return True
            if is_duplicate_in_other_branch(commit, '%s/master' % git_remotenames['mainline']):
                return True
            logging.debug("handling https://git.kernel.org/next/linux-next/c/%s (%s) as a commit with the same subject and author was already present in %s",
                         commit.hexsha[0:13], commit.summary, is_at_least_in_next)
            return False
    return True


def get_latest_mainline_versions():
    latest_versions = []
    for tag in GITPYREPO.git.tag('--sort=-creatordate', '--list', 'v*').splitlines():
        if not latest_versions and re.findall(r'^v[0-9]*.[0-9]*-rc[1-9]$', tag):
            latest_versions.append(tag[0:-4])
        elif re.findall(r'^v[0-9]*.[0-9]*$', tag):
            latest_versions.append(tag)
        if len(latest_versions) > 5:
            break
    return latest_versions


def get_minimum_next_version():
    previous_tag = None
    for tag in GITPYREPO.git.tag('--sort=-creatordate', '--list', 'next*').splitlines():
        commit = GITPYREPO.commit(tag)
        date_commit = datetime.datetime.utcfromtimestamp(commit.authored_date)
        date_distance = DATE_NOW - date_commit
        if date_distance.days < nexttree_age['min']:
            continue
        elif date_distance.days > nexttree_age['max']:
            break
        else:
            previous_tag = tag
    if not previous_tag:
        print('Aborting, could not find a next tag that is at least %s and max %s days old' % (nexttree_age['min'], nexttree_age['max']))
        sys.exit(1)
    return previous_tag


def determine_tag_of_fixed_commit(commit, fixed_commit):
    if not fixed_commit:
        return None
    try:
        tag_of_fixed_commit = GITPYREPO.git.describe(
            '--tags', '--contains', '--abbrev=0', "--exclude=next-*", "--exclude=v*.*.*", fixed_commit)
        return tag_of_fixed_commit.split('-', 1)[0]
    except git.exc.GitCommandError:
        # commit apparently not in mainline yet
        # FIXME: or only merged after the last mainline tag; ignoring this corner-case for now
        pass
    return False


def is_mainline_release_ancient(commit, fixed_commit, tag_of_fixed_commit, latest_mainline_versions):
    for version in latest_mainline_versions:
        if tag_of_fixed_commit.startswith(version):
            return False
    return True

def process():
    for branch in ("%s/master" % git_remotenames['next'], "%s/pending-fixes" % git_remotenames['next'], "%s/master" % git_remotenames['mainline']):
        try:
            GITPYREPO.commit(branch)
        except:
            print("Aborting, branch '%s' not found; maybe the variable 'git_remotenames' needs to be adjusted" % branch)
            sys.exit(1)

    latest_mainline_versions = get_latest_mainline_versions()
    is_at_least_in_next = get_minimum_next_version()
    logging.info('Changes must be in at least %s; mainline cycles considered are %s.' %
                 (is_at_least_in_next, ', '.join(latest_mainline_versions)))

    for commit in GITPYREPO.iter_commits(('--reverse',  '%s/master..%s/master' % (git_remotenames['mainline'], git_remotenames['next']))):
        # check for Fixes: and CC: <stable… tags
        re_fixes = re.findall('^Fixes: ([0-9a-fA-F]*) ', commit.message, re.MULTILINE)
        re_stable = re.search('^C[cC]: .*stable@.*kernel.org', commit.message, re.MULTILINE)
        is_pending = is_part_of_branch(commit, '%s/pending-fixes' % git_remotenames['next'])

        # check if we care about the version the fixes tag points to
        if re_fixes:
            fixed_mainline_version = determine_tag_of_fixed_commit(commit, re_fixes)
            if not fixed_mainline_version:
                logging.debug(
                    'skipping https://git.kernel.org/next/linux-next/c/%s, fixed commit apparently not in mainline yet' % commit.hexsha[0:13])
                continue

            if not is_pending and is_mainline_release_ancient(
                    commit, re_fixes, fixed_mainline_version, latest_mainline_versions):
                logging.debug(
                    "skipping https://git.kernel.org/next/linux-next/c/%s as it's fixing something too old", commit.hexsha[0:13])
                continue
        elif not any((re_stable, is_pending)):
            logging.debug(
                "skipping https://git.kernel.org/next/linux-next/c/%s doesn't contain a fixes or stable tag", commit.hexsha[0:13])
            continue

        # ignore commits that that were not in -next long enough
        if is_too_young(commit, is_at_least_in_next):
            logging.debug(
                "skipping https://git.kernel.org/next/linux-next/c/%s as it was not yet present in %s", commit.hexsha[0:13], is_at_least_in_next)
            continue

        # ignore commits that were merged in a for-next branch first and later cherry-picked to mainline (and thus in both next and mainline)
        if is_duplicate_in_other_branch(commit, '%s/master' % git_remotenames['mainline']):
            logging.debug(
                "skipping https://git.kernel.org/next/linux-next/c/%s commit with same summary in mainline", commit.hexsha[0:13])
            continue

        # prep output string
        tags = []
        if re_fixes:
            tags.append('Fixes [%s]' % fixed_mainline_version)
        if re_stable:
            tags.append('CCed-stable')
        if re.search('^C[cC]: .*regressions@lists.linux.dev', commit.message, re.MULTILINE):
            tags.append('CCed-regressions')
        if re.search(r'^Reported-by:.*\n(Closes|Link)', commit.message, re.MULTILINE):
            tags.append('Reported-by+Closes/Link')
        elif re.search('^Reported-by:', commit.message, re.MULTILINE):
            tags.append('Reported-by')
        elif re.search('^Closes:', commit.message, re.MULTILINE):
            tags.append('Closes')
        if is_pending:
            tags.append('Pending')

        # store result
        result = {
            'commit': commit,
            'tags': ', '.join(tags)
        }

        COMMITS_OF_INTEREST.append(result)

def print_single_result(result):
    print("https://git.kernel.org/next/linux-next/c/%s" % result['commit'].hexsha[0:13], end=' ')
    print(datetime.datetime.utcfromtimestamp(result['commit'].committed_date), end=' ')
    print(result['tags'])
    print(' ', result['commit'].summary)

def print_all_results():
    COMMITS_OF_INTEREST.sort(key=lambda x: x['commit'].committed_date)
    for result in COMMITS_OF_INTEREST:
        print_single_result(result)

if not pathlib.Path('.git').is_dir():
    print("Aborting, no .git dir found")
    sys.exit(1)

GITPYREPO = git.Repo.init()

# iterate through commits in next
process()

# print results
print_all_results()
