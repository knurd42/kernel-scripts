#! /bin/bash2
#
# Copyright (c) 2015-2020 Thorsten Leemhuis <linux@leemhuis.info>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

# git show -s --format=%ci v4.2-rc1^{comm

## configuration
# earliest starting point for linux sources: v2.6.12
if [[ "${1}" ]]; then
	startingpoint="${1}"
else
	startingpoint=v2.6.12
fi

# comment out some of these to speed things up
statsnomergecommits=foo
statsmoved=foo
mergewindowenstats=foo

## checks
#
# improveme: what version is really needed to get proper rename/move detection?
# 1.7.1 from rhel6 is definitely to old
if [[ "${statsmoved}" ]] && git --version | grep 'git version 1.' &> /dev/null; then
	echo "Aborting: Rename detection doesn't work that well with this ancient version of git." 
	exit 1
fi


collectstats() {
	local lastversion=${1}
	local newversion=${2}

	# basisc stats
	commits="$(git log --pretty=oneline ${lastversion}..${newversion} | wc -l)"
	files="$(git ls-tree -r ${newversion} --name-only | wc -l)"
	diffstat="$(git diff --shortstat --no-renames ${lastversion}..${newversion})"
	changedfiles="$(echo "${diffstat}" | cut -d " " -f 2 )"
	insertions="$(echo "${diffstat}" | cut -d " " -f 5 )"
	deletions="$(echo "${diffstat}" | cut -d " " -f 7 )"
	lines="$(git archive --format=tar ${newversion} . | tar -x --to-stdout | wc -l)"

	# count commits w/o merges
	if [[ "${statsnomergecommits}" ]]; then
		commits_nomerges="$(git log --no-merges --pretty=oneline ${lastversion}..${newversion} | wc -l)"
	fi

	# handle rename detection
	if [[ "${statsmoved}" ]]; then
		diffstat_moved="$(git diff -l 999999 -M -C --shortstat ${lastversion}..${newversion})"
		changedfiles_moved="$(echo "${diffstat_moved}" | cut -d " " -f 2 )"
		insertions_moved="$(echo "${diffstat_moved}" | cut -d " " -f 5 )"
		deletions_moved="$(echo "${diffstat_moved}" | cut -d " " -f 7 )"
	fi

	datelast=$(git log -1 --pretty='%cd' --date=unix "${lastversion}")
	datenew=$(git log -1 --pretty='%cd' --date=unix "${newversion}")

	# some dates
	reldate="$(LC_ALL=C date -u --date="@${datenew}")"
	seconds="$(( "${datenew}" - "${datelast}" ))"

	# add date to first columen for HEAD
	if [[ "${newversion}" == "HEAD" ]] ; then
		newversiondesc="HEAD($(git show -s --date=short --format=%cd HEAD))"
	else
		newversiondesc="${newversion}"
	fi

	# output
	echo -n "${newversiondesc} ${lastversion}..${newversion} \"${reldate}\" ${files} ${lines} ${commits}"
	[[ "${statsnomergecommits}" ]] && echo -n " ${commits_nomerges}"
	echo -n " ${changedfiles}"
	[[ "${statsmoved}" ]] && echo -n " ${changedfiles_moved}"
	echo -n " ${insertions}"
	[[ "${statsmoved}" ]] && echo -n " ${insertions_moved}"
	echo -n " ${deletions}"
	[[ "${statsmoved}" ]] && echo -n " ${deletions_moved}"
	echo -n " $((${insertions}-${deletions}))"
	[[ "${statsmoved}" ]] && echo -n " $((${insertions_moved}-${deletions_moved}))"
	echo -n " ${seconds}"

	echo
}

printheader() {
	echo -n "version range reldate Files(after) Lines(after) commits"
	[[ "${statsnomergecommits}" ]] && echo -n " commits(--no-merges)"
	echo -n " files_changed"
	[[ "${statsmoved}" ]] && echo -n " files_changed(-M)"
	echo -n " insertions"
	[[ "${statsmoved}" ]] && echo -n " insertions(-M)"
	echo -n " deletions"
	[[ "${statsmoved}" ]] && echo -n " deletions(-M)"
	echo -n " difference"
	[[ "${statsmoved}" ]] && echo -n " difference(-M)"
	echo -n " seconds"
	echo
}

## go for it
# 
outputmainvers=""
outputmergewin=""

# * git tag -l will show 4.1 before 4.2, so use 'git for-each-ref' instead to get tags
for newversion in $(git for-each-ref --sort=taggerdate --format '%(refname)' refs/tags | sed 's!refs/tags/!!' | grep -E -e '^v2.6\.[0-9]*(-rc1|)$' -e '^v.[0-9]*\.[0-9]*(-rc1|-rc1-dontuse|)*$') HEAD; do
	if [[ ${startingpoint} ]]; then
		if [[ "${newversion}" == "${startingpoint}" ]]; then
			# do not enter this area again
			lastversion=${newversion}			
			unset startingpoint

			# print header now that we got our starting point
			outputmainvers="$(printheader)"
			[[ "${mergewindowenstats}" ]]  && outputmergewin="${outputmainvers}"

			echo -n 'Processing: '
		fi

		# loop until we got our startingpoint
		continue
	fi


	# skip all rc except rc1
	if [[ "${newversion}" == "${newversion%-rc*}" ]]; then
		# this is a mainline relase
		echo -n "${newversion}; " >&2
		outputmainvers="${outputmainvers}"$'\n'"$(collectstats ${lastversion} ${newversion})"
		lastversion=${newversion}
	elif [[ "${mergewindowenstats}" ]] && [[ "${newversion}" == "${newversion%-rc1}"-rc1 ]] ; then
		echo -n "${newversion}; " >&2
		outputmergewin="${outputmergewin}"$'\n'"$(collectstats ${lastversion} ${newversion})"
	elif [[ "${mergewindowenstats}" ]] && [[ "${newversion}" == "${newversion%-rc1-dontuse}"-rc1-dontuse ]] ; then
		#  this handles v5.12-rc1, which had a bad bug and was renamed to v5.12-rc1-dontuse
		#   https://lore.kernel.org/lkml/CAHk-=wjnzdLSP3oDxhf9eMTYo7GF-QjaNLBUH1Zk3c4A7X75YA@mail.gmail.com/
		echo -n "${newversion}; " >&2
		outputmergewin="${outputmergewin}"$'\n'"$(collectstats ${lastversion} ${newversion})"
	else
		# skip other rcs
		continue
	fi
done
echo "Done."
echo

echo "Stats for version from mainline:"
echo "${outputmainvers}"

if [[ "${mergewindowenstats}" ]]; then
	echo
	echo "Stats for merge windows:"
	echo "${outputmergewin}"
fi


