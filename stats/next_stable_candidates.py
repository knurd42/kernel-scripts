# -*- coding: utf-8 -*-
# SPDX-License-Identifier: MIT
# Copyright (C) 2023 by Thorsten Leemhuis
__author__ = 'Thorsten Leemhuis <linux@leemhuis.info>'

# prep
import git
import logging
import re
import sys

from datetime import datetime, timedelta
from difflib import SequenceMatcher

# config
ignore_reverts_since_x_days = 4
ignore_regular_commits_since_x_days = 8
latest_mainline_release = ''
git_repo_dir = ''
git_remote_mainline = 'mainline'
git_remote_next = 'next'
git_remote_stable = 'stable'
logging.basicConfig(level=logging.INFO)
# logging.basicConfig(level=logging.DEBUG)


def gen_backported_commits_list(gitrepo):
    backported_commits = []
    for next_commit in gitrepo.iter_commits(('--ancestry-path', 'v%s..%s/linux-%s.y' % (latest_mainline_release, git_remote_stable, latest_mainline_release))):
        mainline_hexsha = re.findall(
            r'commit ([0-9a-fA-F]{40}) upstream.', next_commit.message)
        if not mainline_hexsha:
            mainline_hexsha = re.findall(
                r'[uU]pstream commit ([0-9a-fA-F]{40})', next_commit.message)
        if not mainline_hexsha:
            if not next_commit.summary.startswith('Linux '):
                logging.debug(
                    'skipping stable commit %s, no upstream commit found in patch description' % next_commit.hexsha[0:12])
            continue
        backported_commits.append(mainline_hexsha[0][0:12])
    return backported_commits


def gen_commits_current_cycle_list(gitrepo):
    commits_current_cycle = []
    for commit in gitrepo.iter_commits(("--since='%s'" % latest_mainline_release, '%s/master' % git_remote_mainline)):
        commits_current_cycle.append(commit.summary)
    return commits_current_cycle


def commit_younger_x_days(commit, offset_days):
    if (datetime.now() - datetime.utcfromtimestamp(commit.committed_date)).days < offset_days:
        logging.debug("skipping %s: commit is less than %s days old",
                      commit.hexsha[0:12], offset_days)
        return True
    return False


def process():
    # table header
    print(','.join(['"summary"', '"CCed stable"', '"reverts commit from this cycle"',
                    '"fixes tag"', '"Reported-by tag"', '"fixed commit backported to linux-%s.y"' % latest_mainline_release,
                    '"authored date"', '"committed date"',
                    'next_hexsha', 'link_next',
                    ]))
    # disabled (see below): '"fixed commit description"',

    gitrepo = git.Repo.init(git_repo_dir)
    backported_commits = gen_backported_commits_list(gitrepo)
    commits_current_cycle = gen_commits_current_cycle_list(gitrepo)
    all_next_commits = []

    for next_commit in gitrepo.iter_commits(('--reverse',  '%s/master..%s/master' % (git_remote_mainline, git_remote_next))):
        all_next_commits.append(next_commit.hexsha[0:12])

        # for the sake of simplicity, just pick the first Fixes: tag
        fixes_re = re.findall(
            r'^Fixes: ([0-9a-fA-F]{12})', next_commit.message, re.MULTILINE)
        fixes = bool(fixes_re)
        cced_stable = bool(
            re.search('^C[cC]: .*stable@.*kernel.org', next_commit.message, re.MULTILINE))
        reported_by = bool(
            re.search('^Reported-by:', next_commit.message, re.MULTILINE))
        # evaluated later:
        fixes_commit_backported = False
        fixes_commit_described = ''
        revert_current_cycle = False

        if next_commit.summary in commits_current_cycle:
            logging.debug(
                "skipping %s: commit with identical subject recently landed in mainline", next_commit.hexsha[0:12])

        if next_commit.summary.startswith('Revert '):
            reverted_commit_summary = next_commit.summary.removeprefix('Revert "')
            reverted_commit_summary = reverted_commit_summary.removesuffix('"')
            if reverted_commit_summary in commits_current_cycle:
                revert_current_cycle = True

        if not any((fixes, cced_stable, revert_current_cycle)):
            logging.debug(
                "skipping %s: neither fixes nor cced_stable", next_commit.hexsha[0:12])
            continue

        if fixes:
            if revert_current_cycle and commit_younger_x_days(next_commit, ignore_reverts_since_x_days):
                continue
            elif commit_younger_x_days(next_commit, ignore_regular_commits_since_x_days):
                continue

            if fixes_re[0] in all_next_commits:
                logging.debug(
                    "skipping %s: commit specified by Fixes: not found in mainline(1)", next_commit.hexsha[0:12])
                continue
            # different, but slower check for same case
            try:
                gitrepo.git.merge_base(
                    '--is-ancestor', fixes_re[0], '%s/master' % git_remote_mainline)
            except:
                logging.debug(
                    "skipping %s: commit specified by Fixes: not found in mainline(2)", next_commit.hexsha[0:12])
                continue

            fixes_commit = gitrepo.commit(fixes_re[0])
            if (datetime.now() - datetime.utcfromtimestamp(fixes_commit.committed_date)).days > 365:
                logging.debug(
                    "skipping %s: fixed commit is more than a year old", next_commit.hexsha[0:12])
                continue

            if fixes_re[0] in backported_commits:
                fixes_commit_backported = True

            # disabled: the "--exclude='next*'" sometimes for some reason doesn't work with gitpython :-/
            #  (or something else is going sideways and I failed to noticed it)
            # try:
            #    fixes_commit_described = gitrepo.git.describe('--tags', '--abbrev=0', "--exclude='next*'", '--contains', fixes_re[0])
            # except:
            #    fixes_commit_described = "post %s" % gitrepo.git.describe('--tags', '--abbrev=0', fixes_re[0])

        authored_date = datetime.utcfromtimestamp(next_commit.authored_date)
        committed_date = datetime.utcfromtimestamp(next_commit.committed_date)

        output = []
        output.append('"%s"' % next_commit.summary.replace('"', '""'))
        output.append('"%s"' % cced_stable)
        output.append('"%s"' % revert_current_cycle)
        output.append('"%s"' % fixes)
        output.append('"%s"' % reported_by)
        output.append('"%s"' % fixes_commit_backported)
        # output.append('"%s"' % fixes_commit_described)
        output.append(authored_date.isoformat())
        output.append(committed_date.isoformat())
        output.append('"%s"' % next_commit.hexsha[0:12])
        output.append(
            'https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=%s' %
            next_commit.hexsha[0:12])
        print(','.join(output))


if not git_repo_dir or not latest_mainline_release:
    if len(sys.argv) != 3:
        logging.error("Please configure script in the header")
        sys.exit(1)
    # this is undocumented
    git_repo_dir = sys.argv[1]
    latest_mainline_release = sys.argv[2]

if latest_mainline_release[0] == 'v':
    latest_mainline_release = latest_mainline_release[1:]

process()
