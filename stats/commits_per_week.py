#!/usr/bin/python3
# coding=utf-8
#
# Copyright (c) 2021 Thorsten Leemhuis <linux@leemhuis.info>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import calendar
import datetime
import git
import sys

# check these years
checkrange_begin = int(datetime.datetime(2006,1,1,0,0,0).timestamp())
checkrange_end = int(datetime.datetime(2021,1,1,0,0,0).timestamp())
checkrange_years = 15

# holds the counters
counter_day = [0]*365
# counter_week = dict.fromkeys(range(1, 53), 0)

# init repo and iterate commits
repo = git.Repo.init(sys.argv[1])
for commit in repo.iter_commits():
#    commit_gmtime = commit.authored_date
    commit_gmtime = commit.committed_date
    commit_date = datetime.datetime.fromtimestamp(commit_gmtime, datetime.timezone.utc)
    commit_timetuple = commit_date.timetuple()
    commit_yday = commit_timetuple.tm_yday

    # simply ignore Feburary 29th
    if calendar.isleap(commit_timetuple.tm_year):
       if commit_timetuple.tm_yday == 60:
          continue
       elif commit_timetuple.tm_yday > 60:
          commit_yday -= 1

    # calculate week and count the last day of the year in week 52
    commit_week = int((commit_yday-1)/7)+1
    if commit_week == 53:
        commit_week = 52

    # now cound
    if commit_gmtime > checkrange_begin and commit_gmtime < checkrange_end:
       # print('Counting: %s %s %s %s %s' % (commit.hexsha, commit_date, commit_yday, commit_week, commit.summary))
       counter_day[commit_yday-1] += 1
       #counter_week[commit_week] += 1

print("day, commits")
for counter, value in enumerate(counter_day):
   day = (datetime.datetime(2019,1,1,0,0,0)+datetime.timedelta(days=counter))
   sevendaysum = counter_day[counter-3] + counter_day[counter-2] + counter_day[counter-1] + \
                     counter_day[counter] + \
                     counter_day[(counter + 1) % len(counter_day)] + counter_day[(counter + 2) % len(counter_day)] + counter_day[(counter + 3) % len(counter_day)]
   average = int(round((sevendaysum / 7 / checkrange_years), 0))
   print('%s %s,%s' % (day.strftime('%b'), day.strftime('%d'), average))
print('')

#print("week_starting_with,commits")
#for week in counter_week.keys():
#   weekstart = (datetime.datetime(2019,1,1,0,0,0)+datetime.timedelta(days=((week*7)-7)))
#   average = int(round((counter_week[week] / checkrange_years), 0))
#   print('%s %s,%s' % (weekstart.strftime('%b'), weekstart.strftime('%d'), average))
#print('')
