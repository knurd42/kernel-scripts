#! /bin/bash
#
# Copyright (c) 2013-2016 Thorsten Leemhuis <linux@leemhuis.info>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

# FIXME:
# reduce list of config options removed to those that are actually in the old kernel
# prevents options from falling through the cracks that were addded and then moved :-)
# REWRITE: this code and the way it works is really really ugly

tmpfile=$(mktemp -t $(basename $0).XXXXXXXXXX) 

# startupchecks
if [[ ${1} ]]; then 
	fromversion="${1}"
else
	echo "Please provide tag with starting point (e.g. v4.5) as first parameter" >&2
	exit 1
fi
if [[ ${2} ]]; then 
	toversion="${2}"
else
	echo "Please provide tag with ending point (e.g. v4.6) as second parameter" >&2
	exit 1
fi
for version in "${fromversion}" "${toversion}"; do
	if ! git tag -l | grep ${version} &> /dev/null ; then
		echo "Could not find ${version} in the output from 'git tag -l'"
		exit 1
	fi
done


if ! tmpfile1="$(mktemp -t $(basename $0).1XXXXXXXXXX)"; then
	echo "Creating a tmpfile failed" >&2
	exit 1
fi
if ! tmpfile2="$(mktemp -t $(basename $0).2XXXXXXXXXX)"; then
	echo "Creating a tmpfile failed" >&2
	exit 1
fi
if ! tmpfile3="$(mktemp -t $(basename $0).3XXXXXXXXXX)"; then
	echo "Creating a tmpfile failed" >&2
	exit 1
fi
if ! tmpfile4="$(mktemp -t $(basename $0).4XXXXXXXXXX)"; then
	echo "Creating a tmpfile failed" >&2
	exit 1
fi
trap "rm -f ${tmpfile1} ${tmpfile2} ${tmpfile3} ${tmpfile4}" EXIT

#
# find all new config options
#
while read line; do
	case ${line} in 
	'+config '*|'+menuconfig '*)
		echo ${line#+} >> "${tmpfile2}"
		;;
	'-config '*|'-menuconfig '*)
		echo ${line#-} >> "${tmpfile3}"
		;;
	esac
done <<< "$(git diff ${fromversion}..${toversion} -- $(git diff --name-only ${fromversion}..${toversion} | grep '/Kconfig$'))"
sort < ${tmpfile2} | uniq > ${tmpfile1}
sort < ${tmpfile3} | uniq > ${tmpfile2}

# now remove options that were just moved
grep -v --word --file "${tmpfile2}" "${tmpfile1}" > "${tmpfile3}"

#
# process
#
for commit in $(git log --format='format:%H' ${fromversion}..${toversion} -- $(git diff --name-only ${fromversion}..${toversion} | grep '/Kconfig$')); do
	git show "${commit}" > ${tmpfile2} 
	if grep -e '^+config' -e '^+menuconfig' ${tmpfile2} | grep -o --file "${tmpfile3}" > ${tmpfile1} ; then
		while read configoption ; do
			# echo ${configoption}
			while read commitline ; do
				# echo ${commitline}
				case ${commitline} in
				'+++ b/'*)
					filename=${commitline#+++ b/}
					;;
				+${configoption}*)
					while read kconfigline; do
						#echo $kconfigline
						#if [[ "${foundoption}" ]]; then
						#	foobar="${foobar}${kconfigline}"$'\n'
						#fi
						case ${kconfigline} in
						${configoption})
							foundoption="${configoption#*config }"
							#foobar="${configoption}"$'\n'
							;;
						config*|menuconfig*)
							if [[ "${foundoption}" ]]; then
								#echo "${filename%Kconfig} ${foundoption} http://git.kernel.org/linus/${commit} (withouthelptext)"
								#foundoption="${filename%Kconfig} ${foundoption} http://git.kernel.org/linus/${commit}"

								echo "${foobar}" >> ${tmpfile4}
								#echo '##########################'

								# unset -- no help text
								unset foundoption
								break 2 # -> to while loop reading configoptions
							fi
							;;
						*help*|*prompt*|*menu*\"*\"*|*comment*\"*\"*|*bool*\"*\"*|*tristate*\"*\"*|string*\"*\"*|hex*\"*\"*|int*\"*\"*)
							# only show those with a help text
							if [[ "${foundoption}" ]]; then
								echo "${filename%Kconfig} ${foundoption} https://git.kernel.org/torvalds/c/${commit}" >> ${tmpfile4}
								unset foundoption

								# optimization: we found what we were looking for, no need to continue
								break 2 # -> to while loop reading configoptions
							fi
							;;
						esac
					done <<< "$(git show ${commit}:${filename})"
					;;
				esac
			done <${tmpfile2}
		done <${tmpfile1}	
	fi
done

sort < ${tmpfile4}
