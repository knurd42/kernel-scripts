# -*- coding: utf-8 -*-
# SPDX-License-Identifier: MIT
# Copyright (C) 2022 by Thorsten Leemhuis
__author__ = 'Thorsten Leemhuis <linux@leemhuis.info>'


# note: this script doesn't perform any checks is this is a proper stable clone


import git
import re
import sys

from datetime import datetime
from difflib import SequenceMatcher


def get_mergecommit(gitrepo, commit):
    try:
        # inspired by https://stackoverflow.com/a/20615706
        ancestry_path = gitrepo.git.rev_list('--ancestry-path', "%s..origin/HEAD" % commit.hexsha)
        first_parent = gitrepo.git.rev_list('--first-parent', "%s..origin/HEAD" % commit.hexsha)

        # handle latest commit, and then find the last result in ancestry_path that's also in first_parent
        if len(ancestry_path) == 0:
            return commit
        for ancestry_hexsha in reversed(ancestry_path.splitlines()):
            if ancestry_hexsha in first_parent:
                return gitrepo.commit(ancestry_hexsha)

    except git.exc.GitCommandError as err:
        errmsg = err.args[2].decode("utf-8")
        print("GitCommandError: {0}".format(errmsg))
        print(err.args)
        return None


def ilterate_commits(directory, verrange):
    processed = []
    gitrepo = git.Repo.init(directory)

    for stable_commit in gitrepo.iter_commits(('--reverse', verrange)):
        mainline_hexsha = re.findall(r'commit ([0-9a-fA-F]{40}) upstream.', stable_commit.message)
        if not mainline_hexsha:
            mainline_hexsha = re.findall(r'[uU]pstream commit ([0-9a-fA-F]{40})', stable_commit.message)
        if not mainline_hexsha:
            if not stable_commit.summary.startswith('Linux '):
                print('skipping %s, no upstream commit found for (%s)' %
                      (stable_commit.hexsha[0:12], stable_commit.summary))
            continue

        mainline_commit = gitrepo.commit(mainline_hexsha[0])
        summary_similarity = SequenceMatcher(None, mainline_commit.summary, stable_commit.summary).ratio()
        if summary_similarity < 0.7:
            print('skipping %s, summary for %s does not really match (```%s``` vs ```%s```; similarity ratio: %s)' % (
                stable_commit.hexsha[0:12], mainline_commit.hexsha[0:12], stable_commit.summary,
                mainline_commit.summary, round(summary_similarity, 3)))
            continue

        fixed_by = bool(re.search('^Fixes: ', stable_commit.message, re.MULTILINE))
        reported_by = bool(re.search('^Reported-by:', stable_commit.message, re.MULTILINE))
        cced_stable = bool(re.search('^C[cC]: .*stable@.*kernel.org', stable_commit.message, re.MULTILINE))
        mainline_merge = get_mergecommit(gitrepo, mainline_commit)

        # reminder: commit and merge in stable are basically at the same time, so no differentiation needed
        mainline_authored_date = datetime.utcfromtimestamp(mainline_commit.authored_date)
        mainline_committed_date = datetime.utcfromtimestamp(mainline_commit.committed_date)
        mainline_merged_date = datetime.utcfromtimestamp(mainline_merge.committed_date)
        stable_merged_date = datetime.utcfromtimestamp(stable_commit.committed_date)

        processed.append({
            'summary': stable_commit.summary,
            'mainline_hexsha': mainline_commit.hexsha[0:12],
            'stable_hexsha': stable_commit.hexsha[0:12],
            'mainline_authored_date': mainline_authored_date,
            'mainline_committed_date': mainline_committed_date,
            'mainline_merged_date': mainline_merged_date,
            'stable_merged_date': stable_merged_date,
            'distance_authored_committed': (mainline_committed_date - mainline_authored_date).days,
            'distance_authored_mainlined': (mainline_merged_date - mainline_authored_date).days,
            'distance_authored_stable': (stable_merged_date - mainline_authored_date).days,
            'distance_committed_stable': (stable_merged_date - mainline_committed_date).days,
            'distance_mainlined_stable': (stable_merged_date - mainline_merged_date).days,
            'fixes': '%s' % fixed_by,
            'reported_by': '%s' % reported_by,
            'cced_stable': '%s' % cced_stable,
        })

    return processed


def print_processed(processed):
    print(','.join(['summary', 'fixes_tag', 'reported_by_tag', 'cced_stable', 'days_authored_committed',
                    'days_authored_mainlined', 'days_authored_stable', 'days_committed_stable',
                    'days_mainlined_stable', 'authored_date', 'committed_date', 'merged_mainline_date',
                    'merged_stable_date', 'mainline_hexsha', 'stable_hexsha', 'link_mainline', 'link_stable']))
    for entry in processed:
        output = []
        output.append('"%s"' % entry['summary'].replace('"', '""'))
        output.append('"%s"' % entry['fixes'])
        output.append('"%s"' % entry['reported_by'])
        output.append('"%s"' % entry['cced_stable'])
        output.append(str(entry['distance_authored_committed']))
        output.append(str(entry['distance_authored_mainlined']))
        output.append(str(entry['distance_authored_stable']))
        output.append(str(entry['distance_committed_stable']))
        output.append(str(entry['distance_mainlined_stable']))
        output.append(entry['mainline_authored_date'].isoformat())
        output.append(entry['mainline_committed_date'].isoformat())
        output.append(entry['mainline_merged_date'].isoformat())
        output.append(entry['stable_merged_date'].isoformat())
        output.append('"%s"' % entry['mainline_hexsha'])
        output.append('"%s"' % entry['stable_hexsha'])
        output.append(
            'https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=%s' %
            entry['mainline_hexsha'])
        output.append(
            'https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/commit/?id=%s' %
            entry['stable_hexsha'])
        print(','.join(output))


if len(sys.argv) != 3:
    print('Provide path to git repository as first parameter and range as second')
    sys.exit(1)
processed = ilterate_commits(sys.argv[1], sys.argv[2])
# processed.sort(key=lambda x: x['distance_committed_stable'], reverse=True)
print_processed(processed)
